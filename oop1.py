class Robot:
    def __init__(self, material="iron", type="machine", kind="for work"): # noqa
        self.material = material
        self.type = type
        self.kind = kind

    def function(self):
        if self.kind == "for work":
            print("Я нужен чтоб работать вместо людей!")
        elif self.kind == "for play":
            print("Я забавный, пошли играть :-)!")
        else:
            print("Моя потребность не понятна, наверное я нужен для красоты...")


class SpotMini(Robot):
    def __init__(self, material, type, kind, color): # noqa
        super().__init__(material, type, kind)
        self.color = color

    def print_color(self):
        print(f"Я робот - модель: SpotMini, я {self.color}, а ты?")


class Atlas(Robot):
    def __init__(self, material, type, kind, feature): # noqa
        super().__init__(material, type, kind)
        self.feature = feature

    def feature_f(self):
        print(f"Я робот - модель: Atlas, у меня есть {self.feature}, я не такой как все!")


class Handle(Robot):
    def __init__(self, material, type, kind, carrying): # noqa
        super().__init__(material, type, kind)
        self.carrying = carrying

    def some_method(self):
        print("Я робот - модель: Handle, мой девиз - Безобразный, неуклюжий, но для склада очень нужный!")


sm1 = SpotMini(material="iron", type="machine", kind="for play", color="красный")
sm1.print_color()
sm1.function()

print()

a1 = Atlas(material="iron", type="machine", kind="kbbkkn", feature="head")
a1.feature_f()
a1.function()

print()

h1 = Handle(material="iron", type="machine", kind="for work", carrying=50)
h1.some_method()
h1.function()
