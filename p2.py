import math


class Shape:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def in_circle(self, point):
        distance = math.sqrt((self.x - point.x) ** 2 + (self.y - point.y) ** 2)
        if distance > self.radius:
            return False
        else:
            return True

    def __contains__(self, point):

        if type(point) is not Point:
            raise TypeError("Point only")

        try:
            return self.in_circle(point)
        except TypeError:
            print("TypeError")


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)


p = Point(5, 5)
# p = int(5)
c = Circle(10, 10, 5)

# print(c.in_circle(p))
print(p in c)
